# -*- coding: utf-8 -*-

def splitHttp(http_data):
    """
    Splits HTTP request or response from HTTP data
    to headers and body and return as a tuple
    """
    http_headers, http_body = '', ''
    http_list = http_data.split('\n')
    for line_num, line in enumerate(http_list):
        if line == '\r' or line == '':
            http_headers = '\n'.join(http_list[:line_num+1])
            http_body = '\n'.join(http_list[line_num+1:])
            break
    return http_headers[:-1], http_body
