# -*- coding: utf-8 -*-
#! /usr/bin/env python
import sys
import users_db
import utils
from borsch import gateway as gw

script, input_str = sys.argv
input_data = eval(input_str)
request = sys.stdin.read()

def headers():
    gw.responseLn('Content-Type: text/html; charset=UTF-8')
    gw.responseLn('')

def Main():
    headers()

    name_db = "talk.db"
    conn, curs = users_db.connCursor(name_db)
    curs.execute('select * from users where login=:login', input_data)
    acc_data = curs.fetchone()
    users_db.commitClose(conn)

    ref = utils.getURLforRedirect(request=request)
    if acc_data and acc_data[-1] == input_data['pwd']:
        s_key = utils.addSession(input_data['login'])
        gw.response('<meta http-equiv="refresh" content="0; url=')
        gw.response(ref)
        if 'index.py' in ref:
            gw.response('?session_key={0}" />'.format(s_key))
        else:
            gw.response('&session_key={0}" />'.format(s_key))
    else:
        gw.responseLn('<meta http-equiv="refresh" content="0 url={0}#login1" />'.format(ref))

if __name__ == '__main__':
    Main()
