#! /usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import utils
from borsch import gateway as gw

script, user_str = sys.argv
input_data = eval(user_str)
request = sys.stdin.read()

def headers():
    gw.responseLn('Content-Type: text/html; charset=UTF-8')
    gw.responseLn('')

#-----------------------------------------------------------------------------
# The function which create file with a name in format "login.extension"
# and write to this new file.
#-----------------------------------------------------------------------------
def writeDataToFile(data, file_ext, login):
    file_name = "avatars/" + str(login) + "." + str(file_ext)
    with open(file_name, 'wb') as f:
        f.write(data)

def Main():
    headers()
    headers = utils.getHeadersDict(request)
    bnry =  utils.getBoundary(headers)
    body = utils.getBodyRequest(request, bnry) #body of request divided by boundary
    ext_file, data_file = utils.getDataFile(body) #extension and data of file
    login = utils.getLogin(input_data['session_key'])
    writeDataToFile(data_file, ext_file, login)

    gw.responseLn('<meta http-equiv="refresh" content="0; url=index.py?session_key={0}" />'.format(input_data['session_key']))

if __name__ == '__main__':
    Main()
