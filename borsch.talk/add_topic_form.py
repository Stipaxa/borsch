from sys import argv
import users_db
import utils
from borsch import gateway as gw

script, input_str = argv
input_data = eval(input_str)
def headers():
    gw.responseLn('Content-Type: text/html; charset=UTF-8')
    gw.responseLn('')

def substVar(line):
    if '$$$ADDTOPICURL$$$' in line:
        line = 'add_topic.py?session_key={0}&theme_id={1}'.format(
                input_data['session_key'], input_data['theme_id'])
    elif '$$$LINKS$$$' in line:
        if 'session_key' in input_data.keys():
            line = '<a href="index.py?session_key={0}">'.format(input_data['session_key']) + 'Home' + '</a>'
            line += ' | '
            line += '<a href="show_topics.py?session_key={0}&theme_id={1}">'.format(input_data['session_key'], input_data['theme_id']) + 'List of Topics'+ '</a>'
        else:
            line = '<a href="index.py">Home</a>'
            line += ' | '
            line += '<a href="show_topics.py?theme_id={0}">'.format(input_data['theme_id']) + 'List of Topics'+ '</a>'
    line = line.replace('\r\n','<br>')
    return line

def Main():  
    headers()
    file_name = 'templates/add_topic_form.html.template'
    with open(file_name) as f:
        for line in f:
            print substVar(line)

if __name__ == '__main__':
    Main()
