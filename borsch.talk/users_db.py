import sys
import sqlite3

#---------------------------------------------------------------------------
# Create connection object that represents the database.
# Here the data will be store in the "user.db" file.
# Return cursor object for call "execute()" method to perfom 
# SQL commands.
#---------------------------------------------------------------------------
def connCursor(name_db):
    conn = sqlite3.connect(name_db)
    c = conn.cursor()
    return conn, c

#---------------------------------------------------------------------------
# Create SQL table "USERS"
#---------------------------------------------------------------------------
def createTableUsers(c):
    c.execute('''CREATE TABLE IF NOT EXISTS USERS(
            Login    TEXT PRIMARY KEY CHECK(TYPEOF(Login) = 'text'),
            Name     TEXT CHECK(TYPEOF(Name) = 'text'),
            Surename TEXT CHECK(TYPEOF(Surename) = 'text'),
            Birthday DATETIME CHECK(DATE(Birthday) IS NOT NULL),
            Password TEXT);''')

def createTableThemes(c):
    c.execute('''CREATE TABLE IF NOT EXISTS THEMES(
                 id INTEGER PRIMARY KEY AUTOINCREMENT,
                 Subject TEXT,
                 User_id TEXT,
                 Creation_date DATETIME);''')
    
def createTableTopics(c):
    c.execute('''CREATE TABLE IF NOT EXISTS TOPICS(
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                id_themes INTEGER,
                Subject TEXT,
                User_id TEXT,
                Creation_date DATETIME,
                Lst_msg_date DATETIME);''')

def createTableMessages(c):
    c.execute('''CREATE TABLE IF NOT EXISTS MESSAGES(
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                id_topic INTEGER,
                Msg TEXT,
                User_id TEXT,
                Creation_date DATETIME);''')

def createTableSession(c):
    c.execute('''CREATE TABLE IF NOT EXISTS SESSIONACT(
                session_key INTEGER,
                login TEXT PRIMARY KEY, 
                lst_timestamp TIMESTAMP);''')


#---------------------------------------------------------------------------
# Drop SQL table "USERS"
#---------------------------------------------------------------------------
def dropTable(c, name_table):
    c.execute('''DROP TABLE IF EXISTS ?;''', name_table)
    
#---------------------------------------------------------------------------
# Add new User to SQL table "USERS"
#---------------------------------------------------------------------------
def addUser(c, user):
    '''
    user is a dictionary that comes from the browser's action form.
    '''
    the_user = (user['login'], user['name'], user['surname'], 
                user['bday'], user['pwd'])
    c.execute('''INSERT INTO USERS 
                 (Login, Name, Surename, Birthday, Password) 
                 VALUES(?,?,?,?,?)''', the_user)
        
#---------------------------------------------------------------------------
# Add new Topic to SQL table "Topics"
#---------------------------------------------------------------------------
def addTopic(c, new_topic):
    the_topic = (new_topic['theme_id'], new_topic['subject'], 
                 new_topic['login'], new_topic['create_date'])
    c.execute('''INSERT INTO TOPICS
                (id_themes, Subject, User_id, Creation_date, Lst_msg_date)
                VALUES(?,?,?,?,?)''', (new_topic['theme_id'], new_topic['subject'], 
                                       new_topic['login'], new_topic['create_date'], new_topic['create_date']))
    return c.lastrowid

#---------------------------------------------------------------------------
# Add new Message to SQL table "Messages"
#---------------------------------------------------------------------------
def addMessage(c, new_message):
    the_message = (new_message['topic_id'], new_message['msg'], 
                   new_message['user_id'], new_message['create_date'])
    c.execute('''INSERT INTO MESSAGES
                (id_topic, Msg, User_id, Creation_date)
                VALUES(?,?,?,?)''', the_message)
    c.execute('''UPDATE TOPICS SET lst_msg_date=? WHERE id=?''', (new_message['create_date'], new_message['topic_id']))

#---------------------------------------------------------------------------
# Add or replace info about user's session
# (session_key, login[hidden from URL], time of last user's activity )
#---------------------------------------------------------------------------
def addSessionRow(c, session):
    the_session = (session['session_key'], session['login'], session['lst_timestamp'])
    # print the_session
    c.execute('''INSERT OR REPLACE INTO SESSIONACT (session_key, login, lst_timestamp)
                VALUES(?,?,?)''', the_session)

#---------------------------------------------------------------------------
# Renew time of last user's session activity
#---------------------------------------------------------------------------
def updateSessionRow(c, s_key, current_time):
    c.execute('''UPDATE SESSIONACT SET lst_timestamp=? WHERE session_key=?''', (current_time, s_key))

#---------------------------------------------------------------------------
# Delete user's session
#---------------------------------------------------------------------------
def deleteSessionRow(c, s_key):
    c.execute('''DELETE FROM SESSIONACT WHERE session_key=?''', (s_key,))

#---------------------------------------------------------------------------
# Change user's details
#---------------------------------------------------------------------------
def updatePersonalDetails(c, user, s_key):
    # the_user = (user['name'], user['surename'], user['birthday'])
    c.execute('''UPDATE USERS SET name=?, surename=?, birthday=? 
                 WHERE login=(SELECT login FROM sessionact WHERE session_key=?)''', (user['name'], user['surename'], user['birthday'], s_key))
    # c.execute('''UPDATE USERS SET
    #                 name = coalesce(nullif(?,''), name),
    #                 surename = coalesce(nullif(?,''), surename),
    #                 birthday = coalesce(nullif(?,''), birthday)
    #             WHERE login=(SELECT login FROM sessionact WHERE session_key=?)''', (user['name'], user['surename'], user['birthday'], s_key))

#---------------------------------------------------------------------------
# Save(commit) the changes.
# Close the connection (we're done with it)
#---------------------------------------------------------------------------
def commitClose(conn):
    conn.commit()
    conn.close()
