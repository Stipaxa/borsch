#! /usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from datetime import datetime, date, timedelta
import users_db
import utils
from borsch import gateway as gw

script, input_str = sys.argv
input_data = eval(input_str)
request = sys.stdin.read()

def headers():
    gw.responseLn('Content-Type: text/html; charset=UTF-8')
    gw.responseLn('')

def getNameTheme(input_data):
    name_db = "talk.db"
    conn, curs = users_db.connCursor(name_db)
    conn.text_factory = str

    curs.execute('select subject from themes where id=?', input_data['theme_id'])
    data = curs.fetchone()
    return data[0]

def getTopics():
    name_db = "talk.db"
    conn, curs = users_db.connCursor(name_db)
    conn.text_factory = str

    # select and sort by "lst_msg_date" coloumn in "topics" table
    curs.execute('select * from topics where id_themes=:theme_id order by lst_msg_date desc', input_data)
    topics = []
    for row in curs:
        topics.append((row[0],row[2])) #list of tuples
    conn.close()
    return topics

def getCountMsg(topic_id):
    name_db = "talk.db"
    conn, curs = users_db.connCursor(name_db)
    curs.execute('select count(*) from messages where id_topic={0}'.format(topic_id))
    num = curs.fetchone()[0]
    conn.close()
    return num

def getLastMsg(topic_id):
    name_db = "talk.db"
    conn, curs = users_db.connCursor(name_db)
    curs.execute('select messages.user_id, max(messages.creation_date), topics.subject, topics.id from messages inner join topics on topics.id=messages.id_topic where topics.id={0}'.format(topic_id))
    last_msg = curs.fetchone()
    conn.close()
    return last_msg

def genTopicsHtml(topic_id, subject):
    html = '<ul class="contentlayer1">'
    if 'session_key' in input_data.keys():
        html += '<li class="" style="width: 400px;"><a href="show_messages.py?session_key={0}&topic_id={1}">'.format(input_data['session_key'], topic_id)
        html += subject + '</a></li>'
    else:
        html += '<li class="" style="width: 400px;"><a href="show_messages.py?topic_id={0}">'.format(topic_id)
        html += subject + '</a></li>'
    num_msg = getCountMsg(topic_id)
    html += '<li class="" style="width: 100px; text-align: center;">' + str(num_msg)
    html += '</li>'
    last_msg = getLastMsg(topic_id)
    day_msg = utils.humanReadableDate(last_msg[1])
    html += '<li class="" style="background: white; width: 350px; color: black;">'
    if str(day_msg).encode('utf-8') == '':
        html += 'N/A'
    else:
        html += str(day_msg).encode('utf-8') + ' '
    if str(last_msg[0]).encode('utf-8') == 'None':
        html += ''
    else:
        html += str(last_msg[0]).encode('utf-8')
    html += '<br></li></ul>'
    html += '<div class="line"></div>'
    return html

def getNewTopicHtml():
    html = '<a href="add_topic_form.py?session_key={0}&theme_id={1}"'.format(input_data['session_key'], input_data['theme_id']) + ' class="button">New Topic</a>'
    return html

def substVar(line):
    if '$$$LOGIN$$$' in line:
        if 'session_key' in input_data.keys():
            line = '<li class="auth" style="float: right;">'
            line += utils.profileModalWindow(input_data['session_key'])
            line += '</li>'
        else:
            line = '<li class="auth" style="float: right;">'
            line += utils.getLoginModalWindows()
            line += '</li>'
    elif '$$$SILO$$$' in line:
        if 'session_key' in input_data.keys():
            line = '<li class="auth" style="float: right;"><a href="logout.py?session_key={0}">Sign Out</a></li>'.format(input_data['session_key'])
        else:
            line = '<li class="" style="float: right;">'
            line += utils.getSigninModalWindows()
    elif '$$$THEME$$$' in line:
        name_theme = getNameTheme(input_data)
        line = name_theme
    elif '$$$TOPICS$$$' in line:
        topics = getTopics()
        line = ''
        for k, v in topics:
            line += genTopicsHtml(k, v)
    elif '$$$NEWTOPIC$$$' in line:
        if 'session_key' in input_data.keys():
            line = getNewTopicHtml()
        else:
            line = ''
    elif '$$$PICT$$$' in line:
        if input_data['theme_id'] == '1':
            line = ' style="background-image: url(images/cat1.jpg);'
            line += ' background-size: 282px; 250px;">'
        elif input_data['theme_id'] == '2':
            line = ' style="background-image: url(images/dog1.jpg);'
            line += ' background-size: 197px; 264px;">'
        elif input_data['theme_id'] == '3':
            line = ' style="background-image: url(images/hamster1.jpg);'
            line += ' background-size: 200px; 235px;">'
    elif '$$$LINKS$$$' in line:
        if 'session_key' in input_data.keys():
            line = '<a href="index.py?session_key={0}" style="text-decoration:underline;">'.format(input_data['session_key']) + 'Home' + '</a>'
        else:
            line = '<a href="index.py" style="text-decoration:underline;">Home</a>'
    line = line.replace('\r\n','<br>')
    return line

def Main():
    if not utils.handleSession(input_data=input_data, on_expired=utils.onExpiredModal, request=request):
        exit(0)
    headers()
    if 'theme_id' in input_data.keys():
        utils.RenderHtmlPage('templates/topics.html.template', substVar)
    else:
        new_url = 'index.py'
        if 'session_key' in input_data.keys():
            new_url += '?session_key={0}'.format(input_data['session_key'])
        gw.responseLn('<meta http-equiv="refresh" content="0; url={0}" />'.format(new_url))
    
if __name__ == '__main__':
    Main()
