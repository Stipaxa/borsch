#! /usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from datetime import datetime
import sqlite3
import users_db
import utils
import show_topics
import percent_decoder
from borsch import gateway as gw

script, input_str = sys.argv
input_data = eval(input_str)
request = sys.stdin.read()

def headers():
    gw.responseLn('Content-Type: text/html; charset=UTF-8')
    gw.responseLn('')

def genTopicHtml():
    html = '<html><body>'
    html += '<a href="index.py?session_key={0}">'.format(input_data['session_key']) + 'Back to Topics</a><br>'
    html += '</body></html>'
    return html

def makeTopic():
    new_sub = percent_decoder.percDecode(input_data['subject'])
    current_time = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S') 
    # Create dict for table Topics.
    new_topic = {}
    new_topic['login'] = utils.getLogin(input_data['session_key'])
    new_topic['theme_id'] = input_data['theme_id']
    new_topic['subject'] = new_sub
    new_topic['create_date'] = current_time 
    return new_topic
    
def makeMsg():
    new_msg = percent_decoder.percDecode(input_data['message'])
    current_time = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S') 
    # Create dict for table Messages.
    new_message = {}
    new_message['msg'] = new_msg
    new_message['user_id'] = utils.getLogin(input_data['session_key'])
    new_message['create_date'] = current_time
    return new_message

def addTopicMsg(new_t, new_m):
    # Work with sql tables.
    # Add rows to Topics and Messages tables.
    name_db = "talk.db"
    conn, curs = users_db.connCursor(name_db)
    conn.text_factory = str
    topic_id = users_db.addTopic(curs, new_t)
    new_m['topic_id'] = topic_id
    users_db.addMessage(curs, new_m)
    users_db.commitClose(conn)
    print "new_m['topic_id']", new_m['topic_id']

def Main():  
    if not utils.handleSession(input_data=input_data, on_expired=utils.onExpiredModal, request=request):
        exit(0)
    headers()
    new_topic = makeTopic()
    new_message = makeMsg()
    addTopicMsg(new_topic, new_message)
    
    gw.responseLn('<meta http-equiv="refresh" content="0; url=show_topics.py?session_key={0}&theme_id={1}" />'.format(input_data['session_key'],input_data['theme_id']))

if __name__ == '__main__':
    Main()
