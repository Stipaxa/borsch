#! /usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from datetime import datetime
import sqlite3
import users_db
import utils
import percent_decoder
from borsch import gateway as gw

script, input_str = sys.argv
input_data = eval(input_str)
request = sys.stdin.read()

def headers():
    gw.responseLn('Content-Type: text/html; charset=UTF-8')
    gw.responseLn('')

def addMessage():
    new_msg = {}
    new_msg['topic_id'] = input_data['topic_id']
    msg_text = percent_decoder.percDecode(input_data['yourcomment'])
    new_msg['msg'] = msg_text
    new_msg['user_id'] = utils.getLogin(input_data['session_key'])
    current_time = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S') 
    new_msg['create_date'] = current_time

    name_db = "talk.db"
    conn, curs = users_db.connCursor(name_db)
    conn.text_factory = str
    users_db.addMessage(curs, new_msg)
    users_db.commitClose(conn)

def Main():  
    if not utils.handleSession(input_data=input_data, on_expired=utils.onExpiredModal, request=request):
        exit(0)
    headers()
    addMessage()
    gw.responseLn('<meta http-equiv="refresh" content="0; url=show_messages.py?session_key={0}&topic_id={1}" />'.format(input_data['session_key'],input_data['topic_id']))

if __name__ == '__main__':
    Main()
