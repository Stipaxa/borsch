# -*- coding: utf-8 -*-
from sys import argv
from datetime import datetime, date, timedelta
import users_db
import utils
import percent_decoder
from borsch import gateway as gw

script, input_str = argv
input_data = eval(input_str)
def headers():
    gw.responseLn('Content-Type: text/html; charset=UTF-8')
    gw.responseLn('')

#Adapt parameters for search
#Cook search parameters
#Bake search parameters
if 'sub' not in input_data:
    input_data['sub'] = ''
if 'datefrom' not in input_data:
    input_data['datefrom'] = ''
if 'dateto' not in input_data:
    input_data['dateto'] = ''
if input_data['datefrom'] or input_data['dateto']:
    if not input_data['datefrom']:
        input_data['datefrom'] = '1970-01-01'
    if not input_data['dateto']:
        input_data['dateto'] = date.today().strftime('%Y-%m-%d')

def decodeInputArgs(input_data):
    input_data['msg'] = percent_decoder.percDecode(input_data['msg'])
    input_data['sub'] = percent_decoder.percDecode(input_data['sub'])
    return input_data

def searchResult(dec_data, op_and=True):
    name_db = "talk.db"
    conn, curs = users_db.connCursor(name_db)
    conn.text_factory = str

    if op_and:
        if dec_data['datefrom'] and dec_data['dateto']:
            curs.execute('''select topics.id, topics.subject, messages.msg, messages.creation_date 
                            from messages inner join topics 
                            on topics.id=messages.id_topic where 
                            (strftime('%Y-%m-%d', messages.creation_date) between :datefrom and :dateto) and
                            instr(topics.subject,:sub) and instr(messages.msg,:msg);''', dec_data)
        else:
            curs.execute('''select topics.id, topics.subject, messages.msg, messages.creation_date 
                            from messages inner join topics 
                            on topics.id=messages.id_topic where 
                            instr(topics.subject,:sub) and instr(messages.msg,:msg);''', dec_data)
    else:
        if dec_data['datefrom'] and dec_data['dateto']:
            curs.execute('''select topics.id, topics.subject, messages.msg, messages.creation_date 
                            from messages inner join topics 
                            on topics.id=messages.id_topic where 
                            (strftime('%Y-%m-%d', messages.creation_date) between :datefrom and :dateto) or
                            instr(topics.subject,:sub) or instr(messages.msg,:msg);''', dec_data)
        else:
            curs.execute('''select topics.id, topics.subject, messages.msg, messages.creation_date 
                            from messages inner join topics 
                            on topics.id=messages.id_topic where 
                            instr(topics.subject,:sub) or instr(messages.msg,:msg);''', dec_data)
    result = curs.fetchall()
    conn.close()
    return result

def substVar(line):
    if '$$$CLARIFY$$$' in line:
        line = '<span style="color: red; font-size:13px; font-family: century gothic, verdana, arial;">'
        line += '<b>Please, clarify parameters of your searching</b></span>'
    elif '$$$ADVASEARCH$$$' in line:
        if 'session_key' in input_data.keys():
            line = '<form method="post" action="search_results.py?session_key={0}" style="position: center;">'.format(input_data['session_key'])
        else:
            line = '<form method="post" action="search_results.py">'
    elif '$$$LINKS$$$' in line:
        if 'session_key' in input_data.keys():
            line = '<a href="index.py?session_key={0}">'.format(input_data['session_key']) + 'Home' + '</a>'
        else:
            line = '<a href="index.py">Home</a>'
    return line

def Main():
    headers()
    dec_data = decodeInputArgs(input_data)
    op_and = True
    if 'simple' in input_data.keys():
        dec_data['sub'] = dec_data['msg']
        op_and = False
    if dec_data['sub'] == '' and dec_data['msg'] == '' and dec_data['datefrom'] == '' and dec_data['dateto'] == '':
        file_name = 'templates/search_form.html.template'
        with open(file_name) as f:
            for line in f:
                print substVar(line)
    else:
        search_result = searchResult(dec_data, op_and)
        if not search_result:
            print '<span style="font-size:13px; font-family: century gothic, verdana, arial;">'
            print 'Not found</span>'
        for row in search_result:
            print '<span style="font-size:13px; font-family: century gothic, verdana, arial;">'
            if 'session_key' in input_data.keys():
                print '<a href="show_messages.py?session_key={0}&topic_id={1}">{2}</a>'.format(input_data['session_key'], row[0], row[1])
            else:
                print '<a href="show_messages.py?topic_id={0}">{1}</a>'.format(row[0], row[1])
            print '<br>'
            print row[3]
            print '<br>'
            print '<span style="word-wrap: break-word;">msg: ', row[2]
            print '</span>'
            print '</span>'
            print '<br><br>'
        #bottom link
        if 'session_key' in input_data.keys():
            print '<br><a href="index.py?session_key={0}" style="font-size:12px; font-family: century gothic, verdana, arial;">Home</a>'.format(input_data['session_key'])
        else:
            print '<br><a href="index.py" style="font-size:12px; font-family: century gothic, verdana, arial;">>Home</a>'

if __name__ == '__main__':
    Main()
