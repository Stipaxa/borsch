#! /usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import users_db
import utils
from borsch import gateway as gw
import sqlite3

script, input_str = sys.argv
input_data = eval(input_str)
request = sys.stdin.read()

def headers():
    gw.responseLn('Content-Type: text/html; charset=UTF-8')
    gw.responseLn('')

def getCountTopics(theme_id):
    name_db = "talk.db"
    conn, curs = users_db.connCursor(name_db)
    curs.execute('select count(*) from topics where id_themes={0}'.format(theme_id))
    num = curs.fetchone()[0]
    conn.close()
    return num

def getCountMsg(theme_id):
    name_db = "talk.db"
    conn, curs = users_db.connCursor(name_db)
    curs.execute('select count(*) from topics inner join messages on topics.id = messages.id_topic where topics.id_themes={0}'.format(theme_id))
    num = curs.fetchone()[0]
    conn.close()
    return num

def getLastMsg(theme_id):
    name_db = "talk.db"
    conn, curs = users_db.connCursor(name_db)
    curs.execute('select messages.user_id, max(messages.creation_date), topics.subject, topics.id from messages inner join topics on topics.id = messages.id_topic where topics.id_themes={0}'.format(theme_id))
    last_msg = curs.fetchone()
    conn.close()
    return last_msg
    
def genThemesHtml(theme_id, subject):
    html = '<ul class="contentlayer1">'
    if 'session_key' in input_data.keys():
        html += '<li class=""><a href="show_topics.py?session_key={0}&theme_id={1}">'.format(input_data['session_key'], theme_id) + subject + '</a></li>'
    else:
        html += '<li class=""><a href="show_topics.py?theme_id={0}">'.format(theme_id) + subject + '</a></li>'
    num_topics = getCountTopics(theme_id)
    html += '<li class="" style="width: 100px; text-align: center;">' + str(num_topics) + '<br></li>'
    num_msg = getCountMsg(theme_id)
    html += '<li class="" style="width: 100px; text-align: center;">' + str(num_msg) + '<br></li>'
    last_msg = getLastMsg(theme_id)
    day_msg = utils.humanReadableDate(last_msg[1])
    html += '<li class="" style="width:350px; color:black; text-align: center;">'
    if 'session_key' in input_data.keys():
        html += '<a href="show_messages.py?session_key={0}&topic_id={1}">'.format(input_data['session_key'], last_msg[3])
    else:
        html += '<a href="show_messages.py?topic_id={0}">'.format(last_msg[3])
    html = u''.join((html,last_msg[2])).encode('utf-8')
    html += '</a>'
    html += '<br>' + day_msg.encode('utf-8') + ' ' + last_msg[0].encode('utf-8') + '<br>' + '</li>'
    html += '</ul>'
    html += '<div class="line"></div>'
    return html 

def getThemes():
    name_db = "talk.db"
    conn, curs = users_db.connCursor(name_db)

    curs.execute('select * from themes')
    theme_dict = {}
    for row in curs:
        theme_dict[row[0]] = row[1]
    conn.close()
    return theme_dict

def substVar(line):
    if '$$$LOGIN$$$' in line:
        if 'session_key' in input_data.keys():
            line = '<li class="auth">'
            line += utils.profileModalWindow(input_data['session_key'])
            line += '</li>'
        else:
            line = '<li class="" style="float: left;">'
            line += utils.getLoginModalWindows()
            line += '</li>'
    elif '$$$SILO$$$' in line:
        if 'session_key' in input_data.keys():
            line = '<li class="auth"><a href="logout.py?session_key={0}">Sign Out</a></li>'.format(input_data['session_key'])
        else:
            line = '<li class="" style="float: left;">'
            line += utils.getSigninModalWindows()
    elif '$$$SEARCH' in line:
        if 'session_key' in input_data.keys():
            line = '<form method="post" action="search_results.py?session_key={0}&simple=yes">'.format(input_data['session_key'])
        else:
            line = '<form method="post" action="search_results.py?simple=yes">'
    elif '$$$ADVASEARCH$$$' in line:
        if 'session_key' in input_data.keys():
            line = '<a href="search_form.py?session_key={0}" style="font-size:12px;">advanced_search</a>'.format(input_data['session_key'])
        else:
            line = '<a href="search_form.py" style="font-size:12px;">advanced_search</a>'
    elif '$$$THEMES$$$' in line:
        theme_dict = getThemes()
        line = ''
        for k, v in theme_dict.iteritems():
            line += genThemesHtml(k, v)
    return line

def Main():
    if not utils.handleSession(input_data=input_data, on_expired=utils.onExpiredModal, request=request):
        exit(0)
    headers()
    utils.RenderHtmlPage('templates/index.html.template', substVar)
        
if __name__ == '__main__':
    Main()
