# -*- coding: utf-8 -*-
from sys import argv
from datetime import datetime, date, timedelta
import users_db
import utils
from borsch import gateway as gw

script, input_str = argv
input_data = eval(input_str)
def headers():
    gw.responseLn('Content-Type: text/html; charset=UTF-8')
    gw.responseLn('')

def substVar(line):
    if '$$$CLARIFY$$$' in line:
        line = ''
    if '$$$SUBSEARCH$$$' in line:
        if 'session_key' in input_data.keys():
            line = '<form method="post" action="search_results.py?session_key={0}">'.format(input_data['session_key'])
        else:
            line = '<form method="post" action="search_results.py">'
    elif '$$$ADVASEARCH$$$' in line:
        if 'session_key' in input_data.keys():
            line = '<form method="post" action="search_results.py?session_key={0}" style="position: center;">'.format(input_data['session_key'])
        else:
            line = '<form method="post" action="search_results.py">'
    elif '$$$LINKS$$$' in line:
        if 'session_key' in input_data.keys():
            line = '<a href="index.py?session_key={0}">'.format(input_data['session_key']) + 'Home' + '</a>'
        else:
            line = '<a href="index.py">Home</a>'
    return line

def Main():  
    headers()
    file_name = 'templates/search_form.html.template'
    with open(file_name) as f:
        for line in f:
            print substVar(line)

if __name__ == '__main__':
    Main()
