#! /usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import sqlite3
import hashlib
from datetime import datetime, date, timedelta
from borsch import gateway as gw
import users_db

SESSION_OK = 0
SESSION_EXPIRED = 1
SESSION_INVALID = 2

def RenderHtmlPage(template, substFunc):
    with open(template) as f:
        for line in f:
            print substFunc(line)

def humanReadableDate(a_datetime):
    """
    This function takes a_datetime as a str
    and return human readable representation of it.
    Format of a_datetime is the following: YYYY-MM-DD hh:mm:ss
    """
    if not a_datetime:
        return ''
    today = str(date.today())
    yesterday = str(date.today() - timedelta(1))
    a_date, a_time = a_datetime.split(' ')
    if a_date == today:
        return 'Today ' + a_time
    elif a_date == yesterday:
        return 'Yesterday ' + a_time
    else:
        return a_datetime

#=================- Work with modal windows -=======================
def profileModalWindow(s_key):
    login = getLogin(s_key)
    profile_details = getProfileDetails(s_key)

    line = '<a href="#profile" class="" style="color:white;">{0}</a>'.format(login)
    line += '<a href="#x" class="overlay" id="profile"></a>'
    line += '<div class="popup">'
    line += '<form method="post" action="profile_form.py?session_key={0}">'.format(s_key)
    line += '<p id="zzz"></p>'
    line += '<h2>Profile details:</h2>'

    for k, v in profile_details.items():
        if k == 'login':
            line += '{0}: {1} <br><br>'.format(k,v)
        elif k == 'password':
            pass
        else:
            line += '<div>'
            line += '{0}: '.format(k)
            if k == 'birthday':
                line += '<input type="date" name="{0}" value="{1}" />'.format(k,v)
            else:
                line += '<input type="text" name="{0}" value="{1}" />'.format(k,v)
            line += '<br><br></div>'
    line += '<input type="submit" value="Save">'

    line += '</form>'
    line += '<a class="close" title="Close" href="#close"></a>'
    line += '</div>'
    return line

def loginModalWindow(link='login', message='', set_href=True):
    if set_href:
        line = '<a href="#{0}" class="" style="color:white;">Sign In</a>'.format(link)
    else:
        line = ''
    line += '<a href="#x" class="overlay" id="{0}"></a>'.format(link)
    line += '<div class="popup">'
    line += '<form method="post" action="login.py">'
    if message:
        line += message
    line += '<h2>Please, enter your details:</h2>'
    line += 'Name: <span style="padding-left: 25px; background: white;"></span><input type="text" name="login" autofocus/><br><br>'
    line += 'Password: <input type="password" name="pwd"><br><br>'
    line += '<input type="submit" class="subm" value="Sign In">'
    line += '</form>'
    line += '<a class="close" title="Close" href="#close"></a>'
    line += '</div>'
    return line

def getLoginModalWindows():
    line =  signinModalWindow(link='signin', message='', set_href=True)
    line += signinModalWindow(link='signin1', message='User already exist', set_href=False)
    line += signinModalWindow(link='signin2', message='Full all fields', set_href=False)
    return line

def signinModalWindow(link='signin', message='', set_href=True):
    if set_href:
        line = '<a href="#{0}" class="" style="color:white;">Sign Up</a>'.format(link)
    else:
        line = ''
    line += '<a href="#x" class="overlay" id="{0}"></a>'.format(link)
    line += '<div class="popup">'
    line += '<form method="post" action="register.py">'
    if message:
        line += message
    line += '<h2>Please, enter your details:</h2>'
    line += 'Login: <span style="padding-left: 31px;"></span><input type="text" name="login" autofocus="autofocus"/><br><br>'
    line += 'Name: <span style="padding-left: 27px;"></span><input type="text" name="name"/><br><br>'
    line += 'Surname: <span style="padding-left: 5px;"></span><input type="text" name="surname"><br><br>'
    line += 'Birthday: <span style="padding-left: 9px;"></span><input type="date" name="bday"/><br><br>'
    line += 'Password: <input type="password" name="pwd"><br><br>'
    line += '<input type="submit" class="subm" value="Sign Up">'
    line += '</form>'
    line += '<a class="close" title="Close" href="#close"></a>'
    line += '</div>'
    return line

def getSigninModalWindows():
    line =  loginModalWindow(link='login', message='', set_href=True)
    line += loginModalWindow(link='login1', message='Username or Password<br>are not correct', set_href=False)
    line += loginModalWindow(link='login2', message='Expired session', set_href=False)
    return line


#=================- Work with session key -=======================
def generateSessionKey(login):
    current_time = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S') 
    h = hashlib.sha1(login)
    h.update(current_time)
    hash_sum = h.hexdigest()
    return hash_sum

def addSession(login):
    current_time = datetime.utcnow() 
    hash_sum = generateSessionKey(login)
    session_data = {}
    session_data['session_key'] = hash_sum
    session_data['login'] = login
    session_data['lst_timestamp'] = current_time

    name_db = "talk.db"
    conn, curs = users_db.connCursor(name_db)
    conn.text_factory = str
    users_db.addSessionRow(curs, session_data)
    users_db.commitClose(conn)
    return session_data['session_key']

def updateSession(s_key):
    current_time = datetime.utcnow() 
    
    name_db = "talk.db"
    conn, curs = users_db.connCursor(name_db)
    users_db.updateSessionRow(curs, s_key, current_time)
    users_db.commitClose(conn)

def deleteSession(s_key):
    name_db = "talk.db"
    conn, curs = users_db.connCursor(name_db)
    users_db.deleteSessionRow(curs, s_key)
    users_db.commitClose(conn)

def getLogin(s_key):
    name_db = "talk.db"
    conn, curs = users_db.connCursor(name_db)
    curs.execute('select login from sessionact where session_key=(?)', (s_key,))
    login = curs.fetchone()[0]
    users_db.commitClose(conn)
    return login

def getProfileDetails(s_key):
    name_db = "talk.db"
    conn, curs = users_db.connCursor(name_db)
    curs.execute('''select users.login, users.name, users.surename, users.birthday, users.password from users 
                    inner join sessionact on users.login=sessionact.login where sessionact.session_key="{0}"'''.format(s_key))
    profile_details = {}
    pd = curs.fetchone()
    if pd:
        profile_details.update({'login'   :pd[0], 
                                'name'    :pd[1], 
                                'surename':pd[2], 
                                'birthday':pd[3],
                                'password':pd[4]})
    users_db.commitClose(conn)
    return profile_details

def checkSessionStatus(s_key):
    dt = timedelta(minutes=1)
    current_time = datetime.utcnow() 

    name_db = "talk.db"
    conn = sqlite3.connect(name_db, detect_types=sqlite3.PARSE_DECLTYPES)
    curs = conn.cursor()
    curs.execute('select lst_timestamp from sessionact where session_key=(?)', (s_key,))
    record = curs.fetchone()
    if record:
        lst_time = record[0]
    else:
        users_db.commitClose(conn)
        return SESSION_INVALID
    users_db.commitClose(conn)
    if (current_time - lst_time) < dt:
        return SESSION_OK
    else:
        return SESSION_EXPIRED

def checkAndUpdateSession(s_key):
    status = checkSessionStatus(s_key)
    if status == SESSION_OK:
        updateSession(s_key)
    elif status == SESSION_EXPIRED:
        deleteSession(s_key)
    return status

def onInvalidDefault(**kwargs):
    gw.responseLn('HTTP/1.1 500 Internal Server Error')
    gw.responseLn('Content-Type: text/html; charset=UTF-8')
    gw.responseLn('')

def onExpiredModal(**kwargs):
    url = getURLforRedirect(**kwargs)
    url = getUrlWithoutSK(url)
    gw.responseLn('')
    gw.responseLn('<meta http-equiv="refresh" content="0; url={0}#login2" />'.format(url))

#-----------------------------------------------------------------------------
# The function which returns URL without session_key.
#-----------------------------------------------------------------------------
def getUrlWithoutSK(url_with_args):
    if '?' in url_with_args:
        url_tmp, args = url_with_args.split('?')
    else:
        return url_with_args
        exit(0)
    args_without_sk = [i for i in args.split('&') if not i.startswith('session_key')]
    new_args = '&'.join(args_without_sk)
    if new_args == '':
        new_url = url_tmp
    else:
        new_url = url_tmp + '?' + new_args
    return new_url

def getURLforRedirect(**kwargs):
    request = kwargs['request']
    if 'Referer:' in request:
        new_url = getReferer(request)
    else:
        line = request.split('\r\n')[0]
        url_with_args = line.split(' ')[1][1:]
        new_url = url_with_args 
    return new_url

#on_expired=onExpiredModal, on_invalid=onInvalidDefault - are 2 function
def handleSession(**kwargs):
    input_data = kwargs['input_data']
    on_expired = kwargs.get('on_expired', onExpiredModal)
    on_invalid = kwargs.get('on_invalid', onInvalidDefault)
    if 'session_key' in input_data.keys():
        # options: return 
        # 0 - valid session => normal behaviour (not minus!!!) => render the page
        # 1 - session id is okay but time has been expired => make him login
        # 2 - session id not found => return auth error
        valid_session = checkAndUpdateSession(input_data['session_key'])
        if valid_session == SESSION_OK:
            return True
        elif valid_session == SESSION_EXPIRED:
            on_expired(**kwargs)
            return False
        else:
            on_invalid(**kwargs)
            return False
    return True

#=================- Get headers and data from request -=======================
#-----------------------------------------------------------------------------
# The function which returns headers of request in dictionary format.
#-----------------------------------------------------------------------------
def getHeadersDict(req):
    hdrs = req.split('\r\n')[1:]
    hdr_dict = {}
    for line in hdrs:
        if line == '\r' or line == '':
            break
        line_tmp = line.split(': ')
        k = line_tmp[0]
        v = line_tmp[1]
        hdr_dict[k] = v
    return hdr_dict 

#-----------------------------------------------------------------------------
# The function which returns boundary.
#-----------------------------------------------------------------------------
def getBoundary(hrd_dict):
    cnt_type_line = hrd_dict['Content-Type']
    boundary = cnt_type_line.split('boundary=')[1]
    return boundary

#-----------------------------------------------------------------------------
# The function which returns request's body.
#-----------------------------------------------------------------------------
def getBodyRequest(request, bndry):
    boundary = '--' + bndry
    body = request.split(boundary)[1]
    return body

#-----------------------------------------------------------------------------
# The function which returns data and extension of a future file.
#-----------------------------------------------------------------------------
def getDataFile(body):
    split_body = body.split('\r\n\r\n')
    data = split_body[1]
    hdrs_body = split_body[0]
    hdr_line = hdrs_body.split('\r\n')[2] #Content-Type: image/gif
    cnt = hdr_line.split('Content-Type: ')[1] #image/gif
    file_ext = cnt.split('/')[1]
    return file_ext, data

#-----------------------------------------------------------------------------
# The function which returns URL previous page.
#-----------------------------------------------------------------------------
def getReferer(request):
    req_dict = getHeadersDict(request)
    return req_dict['Referer']

#-----------------------------------------------------------------------------
# The function which write dictionary "d" to file "file_name"
#-----------------------------------------------------------------------------
def saveDictToFile(d, file_name):
    with open(file_name, 'w') as f:
        for k, v in d.items():
            line = '{0}, {1}\r\n'.format(k,v)
            f.write(line)
    f.close()

