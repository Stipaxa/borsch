# -*- coding: utf-8 -*-
import sys
from borsch import gateway as gw

def Main():
    gw.responseLn("HTTP/1.1 302 Found")
    gw.responseLn("Location: index.py")
    gw.responseLn("\r\n")
        
if __name__ == '__main__':
    Main()
