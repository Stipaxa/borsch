# -*- coding: utf-8 -*-
#! /usr/bin/env python
import sys
import os
import sqlite3
import users_db
import utils
from borsch import gateway as gw

STATUS_ONLINE  = 3
STATUS_OFFLINE = 4

script, input_str = sys.argv
input_data = eval(input_str)
request = sys.stdin.read()

def headers():
    gw.responseLn('Content-Type: text/html; charset=UTF-8')
    gw.responseLn('')

def getMessages():
    name_db = "talk.db"
    conn, curs = users_db.connCursor(name_db)
    curs.execute('select * from topics where id=:topic_id', input_data)
    msg_dict = {}
    for row in curs:
        msg_dict[row[1]] = row[2]
    conn.close()
    return msg_dict

def getTopicSubject():
    name_db = "talk.db"
    conn, curs = users_db.connCursor(name_db)
    conn.text_factory = str
    curs.execute('select Subject from topics where id=:topic_id', input_data)
    data = curs.fetchone()
    conn.close()
    return data[0] 

def getThemeId():
    name_db = "talk.db"
    conn, curs = users_db.connCursor(name_db)
    conn.text_factory = str
    curs.execute('select topics.id_themes, themes.subject from topics inner join themes on topics.id_themes=themes.id where topics.id=:topic_id', input_data)
    data = curs.fetchone()
    conn.close()
    return data 

def getFilesAvatar(user):
    mtime = lambda f: os.stat(os.path.join('avatars', f)).st_mtime
    lst = list(sorted(os.listdir('avatars'), key=mtime, reverse=True))
    avatars = [s for s in lst if s.startswith(user)]
    return avatars

def getLastAvatar(user):
    lst_avts = getFilesAvatar(user)
    if lst_avts:
        return lst_avts[0]
    else:
        return 'default.png'

def checkOnlineStatus(user):
    name_db = "talk.db"
    conn, curs = users_db.connCursor(name_db)
    curs.execute('select session_key from sessionact where login=(?)', (user,))
    online_status = curs.fetchone()
    users_db.commitClose(conn)
    if online_status:
        return STATUS_ONLINE
    else:
        return STATUS_OFFLINE

def getSingleMsgHtml(date, number, user, msg):
    html = '<ul class="user">'
    html += '<li class="" style="width: 200px;">' + date + '</li>'
    html += '<li class="" style="text-align: right;">' + '#' + str(number) + '</li>'
    html += '</ul>'
    html += '<ul class="comment">'
    html += '<li class="" style="width: 200px;">' + user 
    html += '<br><br>'
    html += '<img src="avatars/' + getLastAvatar(user) + '"' + ' alt="avatar"/><br><br>' 
    online_status = checkOnlineStatus(user)
    html += '<img src="images/'
    if online_status == STATUS_ONLINE:
        html += 'user_online.png" title="User ' + user + ' is online now"' 
    else:
        html += 'user_offline.png" title="User ' + user + ' is offline"'
    html += ' />'
    html += '</li>'
    html += '<li class="">' + msg + '</li>'
    html += '</ul>'
    return html

def getAllMsgHtml():
    name_db = "talk.db"
    conn, curs = users_db.connCursor(name_db)
    conn.text_factory = str
    curs.execute('select * from messages where id_topic=:topic_id', input_data)
    i = 1
    html = ''
    for msg in curs:
        html += getSingleMsgHtml(msg[4], i, msg[3], msg[2])
        i += 1
    conn.close()
    return html

def substVar(line):
    id_theme, subject = getThemeId()
    if '$$$LOGIN$$$' in line:
        if 'session_key' in input_data.keys():
            line = '<li class="auth" style="float: right; margin-right: 20px;">'
            line += utils.profileModalWindow(input_data['session_key'])
            line += '</li>'
        else:
            line = '<li class="auth" style="float: right; margin-right: 20px;">'
            line += utils.getLoginModalWindows()
            line += '</li>'
    elif '$$$SILO$$$' in line:
        if 'session_key' in input_data.keys():
            line = '<li class="auth" style="float: right;"><a href="logout.py?session_key={0}">Sign Out</a></li>'.format(input_data['session_key'])
        else:
            line = '<li class="" style="float: right;">'
            line += utils.getSigninModalWindows()
    elif '$$$MAIN$$$' in line:
        if 'session_key' in input_data.keys():
            line = '<a href="index.py?session_key={0}" style="text-decoration: underline;">'.format(input_data['session_key']) + 'Borsch.talk' + '</a>'
        else:
            line = '<a href="index.py" style="text-decoration: underline;">Borsch.talk</a>'
    elif '$$$TOPICS$$$' in line:
        if 'session_key' in input_data.keys():
            line = '<a href="show_topics.py?session_key={0}&theme_id={1}" style="text-decoration: underline;">'.format(input_data['session_key'], id_theme) + subject + '</a>'
        else:
            line = '<a href="show_topics.py?theme_id={0}" style="text-decoration: underline;">'.format(id_theme) + subject + '</a>'
    elif '$$$SUBJECT$$$' in line:
        line = '<span style="text-decoration: underline; font-size: 24px;">' + getTopicSubject() + '</span>'
    elif '$$$MESSAGES$$$' in line:
        line = getAllMsgHtml()
    elif '$$$ADDMESSAGE$$$' in line:
        if 'session_key' in input_data.keys():
            line = '<form name="inputcomment" method="post" action='  
            line += 'add_message.py?session_key={0}&topic_id={1}>'.format(input_data['session_key'], input_data['topic_id']) 
            line += '''<p style="padding-bottom: 5px;">Your comment</p>
                       <p><textarea name="yourcomment" cols="120" rows="7">'''
            line += '''</textarea></p>
                        <p><input type="submit" value="Send">
                        <input type="reset" value="Reset"></p>
                        </form>'''
        else:
            line = '<br><br><br>'
    elif '$$$LINKS$$$' in line:
        if 'session_key' in input_data.keys():
            line = '<a href="index.py?session_key={0}">'.format(input_data['session_key']) + 'Home' + '</a>'
            line += ' | '
            line += '<a href="show_topics.py?session_key={0}&theme_id={1}">'.format(input_data['session_key'], id_theme) + 'List of Topics'+ '</a>'
        else:
            line = '<a href="index.py">Home</a>'
            line += ' | '
            line += '<a href="show_topics.py?theme_id={0}">'.format(id_theme) + 'List of Topics'+ '</a>'
    line = line.replace('\r\n','<br>')
    return line

def Main():  
    if not utils.handleSession(input_data=input_data, on_expired=utils.onExpiredModal, request=request):
        exit(0)
    headers()

    if 'topic_id' in input_data.keys():
        utils.RenderHtmlPage('templates/messages.html.template', substVar)
    else:
        new_url = 'index.py'
        if 'session_key' in input_data.keys():
            new_url += '?session_key={0}'.format(input_data['session_key'])
        gw.responseLn('<meta http-equiv="refresh" content="0; url={0}" />'.format(new_url))


if __name__ == '__main__':
    Main()
