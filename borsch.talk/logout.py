# -*- coding: utf-8 -*-
#! /usr/bin/env python
import sys
from sys import argv
from borsch import gateway as gw
import users_db
import utils

script, input_str = argv
input_data = eval(input_str)
request = sys.stdin.read()

def headers():
    gw.responseLn('Content-Type: text/html; charset=UTF-8')
    gw.responseLn('')

def Main():
    headers()
    ref = utils.getURLforRedirect(request=request)
    new_url = utils.getUrlWithoutSK(ref)
    gw.response('<meta http-equiv="refresh" content="0; url={0}" />'.format(new_url))
    utils.deleteSession(input_data['session_key'])

if __name__ == '__main__':
    Main()
