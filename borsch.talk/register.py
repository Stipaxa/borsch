# -*- coding: utf-8 -*-
#! /usr/bin/env python
import sys
import subprocess
import sqlite3
import users_db
import utils
from borsch import gateway as gw

script, user_str = sys.argv
user = eval(user_str)
request = sys.stdin.read()

def headers():
    gw.responseLn('Content-Type: text/html; charset=UTF-8')
    gw.responseLn('')
#print user
# user = {
#     'login': 'Sti',
#     'name': 'Taya',
#     'surname': 'Yurievna',
#     'bday': '1980-11-23',
#     'pwd': 'bla'
# }

def Main():
    headers()
    ref = utils.getURLforRedirect(request=request)

    for i in user.values():
        if i == '':
            gw.responseLn('<meta http-equiv="refresh" content="0 url={0}#signin2" />'.format(ref))
            exit(0)

    name_db = "talk.db"
    conn, curs = users_db.connCursor(name_db)
    try:
        users_db.addUser(curs, user) 
        users_db.commitClose(conn)
        s_key = utils.addSession(user['login'])
        gw.response('<meta http-equiv="refresh" content="0; url=')
        gw.response(ref)
        if 'index.py' in ref:
            gw.response('?session_key={0}" />'.format(s_key))
        else:
            gw.response('&session_key={0}" />'.format(s_key))
    except sqlite3.IntegrityError:
        gw.responseLn('<meta http-equiv="refresh" content="0 url={0}#signin1" />'.format(ref))


if __name__ == '__main__':
    Main()
