function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
  results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function removeParam(key, sourceURL) {
  var rtn = sourceURL.split("?")[0],
      param,
      params_arr = [],
      queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
  if (queryString !== "") {
    params_arr = queryString.split("&");
    for (var i = params_arr.length - 1; i >= 0; i -= 1) {
      param = params_arr[i].split("=")[0];
      if (param === key) {
      params_arr.splice(i, 1);
      }
    }
    rtn = rtn + "?" + params_arr.join("&");
  }
  return rtn;  
}

function modalWins() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      //document.getElementById("zzz").innerHTML = "Good";
      document.location.hash = "#profile";
    }
    if (this.readyState == 4 && this.status == 500) {
      var del_req = new XMLHttpRequest();
      var session_key = getParameterByName("session_key", document.location);
      del_req.open("GET", "delete_session_ajax.py?session_key=" + session_key, true);
      del_req.send();
      var ind = "index.py"
      url = document.location.href
      if (url.includes(ind)) {
        new_url = "index.py#login2"
      }
      else {
        new_url = removeParam("session_key", document.location.href) + "#login2"
      }
      document.location.assign(new_url)
    }
  };
  var session_key = getParameterByName("session_key", document.location);
  xhttp.open("GET", "extend_session_ajax.py?session_key=" + session_key, true);
  xhttp.send();
}

