#! /usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import users_db
import utils
from borsch import gateway as gw
import sqlite3

script, user_str = sys.argv
input_data = eval(user_str)
request = sys.stdin.read()

def headers():
    gw.responseLn('Content-Type: text/html; charset=UTF-8')
    gw.responseLn('')

def profileDetails():
    user = {}
    if 'name' in input_data.keys():
        user['name'] = input_data['name']
    else:
        user['name'] = ''
    if 'surename' in input_data.keys():
        user['surename'] = input_data['surename']
    else:
        user['surename'] = ''
    if 'birthday' in input_data.keys():
        user['birthday'] = input_data['birthday']
    else:
        user['birthday'] = ''
    return user

def Main():
    if not utils.handleSession(input_data=input_data, on_expired=utils.onExpiredModal, request=request):
        exit(0)
    headers()
    user = profileDetails()
    name_db = "talk.db"
    conn, curs = users_db.connCursor(name_db)
    conn.text_factory = str
    users_db.updatePersonalDetails(curs, user, input_data['session_key'])
    users_db.commitClose(conn)

    new_url = utils.getURLforRedirect(request=request)
    gw.responseLn('<meta http-equiv="refresh" content="0; url={0}" />'.format(new_url))

if __name__ == '__main__':
    Main()
