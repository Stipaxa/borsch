# -*- coding: utf-8 -*-
#! /usr/bin/env python
import sys
from datetime import datetime
sys.path.append('/home/Stipaxa/dev/borsch/borsch.talk/')
import utils
from borsch import gateway as gw

script, input_str = sys.argv
input_data = eval(input_str)
request = sys.stdin.read()

def Main():
    headers_d = utils.getHeadersDict(request)
    try:
        key, value = headers_d['Cookie'].split('=')
        if key != 'session_key':
            raise ValueError
        login = utils.getLogin(value)
        str_out = '<p style="color:green;font-weight:bold;">Logged as ' + login + '<br>'
        str_out += '{}'.format(headers_d['Cookie'])
    except:
        str_out = '<p style="color:red;font-weight:bold;">Not Logged in'
    str_out += '<br>' + str(datetime.now())
    str_out += '</p>'

    gw.responseLn('HTTP/1.1 200 OK')
    gw.responseLn('Content-Type: text/html; charset=UTF-8')
    gw.responseLn('')
    gw.responseLn(str_out)

if __name__ == '__main__':
    Main()
