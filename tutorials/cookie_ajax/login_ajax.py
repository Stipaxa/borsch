# -*- coding: utf-8 -*-
#! /usr/bin/env python
import sys
sys.path.append('/home/Stipaxa/dev/borsch/borsch.talk/')
import users_db
import utils
from borsch import gateway as gw

script, input_str = sys.argv
input_data = eval(input_str)
request = sys.stdin.read()

def Main():
    name_db = "talk.db"
    conn, curs = users_db.connCursor(name_db)
    curs.execute('select * from users where login=:login', input_data)
    acc_data = curs.fetchone()
    users_db.commitClose(conn)

    if acc_data and acc_data[-1] == input_data['pwd']:
        s_key = utils.addSession(input_data['login'])
        gw.responseLn('HTTP/1.1 200 OK')
        gw.responseLn('Set-Cookie: session_key={0}; Max-Age=60;'.format(s_key))
    else:
        gw.responseLn('HTTP/1.1 401 Unauthorized')

if __name__ == '__main__':
    Main()
