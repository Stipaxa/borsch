# -*- coding: utf-8 -*-
#! /usr/bin/env python
import sys
import users_db
import utils
from borsch import gateway as gw

script, input_str = sys.argv
input_data = eval(input_str)
request = sys.stdin.read()

def Main():
    gw.responseLn('HTTP/1.1 200 OK')
    gw.responseLn('Content-Type: text/html; charset=UTF-8')
    gw.responseLn('')

    gw.response(request.replace('\r\n', '<br>'))

if __name__ == '__main__':
    Main()
