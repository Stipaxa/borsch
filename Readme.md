#Project Borsch
Learning project written in Python which consists of:

* borsch.www - HTTP server; 
* borsch.talk - web application running on this server.

This is a learning project so the goals are to implement all the functionality from scratch, using minimum amount of dependencies.

#Licence
Todo insert licence agreement text. MIT.

#Features and limitations
##borsch.www

* supported methods GET, POST.
* dynamic html pages: python language using borsch.gateway interface.
* static html pages.
* list of MIME types: text/html, image/x-icon, image/png, image/gif, image/jpeg, text/css.
* up to 5 connections at the same time (hardcoded).
* IP address and port of the server are hardcoded (0.0.0.0:80).
* HTTP/1.1 only.
* keep-alive not supported, server closes every connection.
* hardcoded 404 page.

##borsch.talk

* web forum written in Python and connected to the borsch.www with borsch.gateway interface.
* new user registration.
* login state: add new topics and new messages.
* not login state: read only.
* sqlite3 as database.
* static background and static mimimi pics for decorations.
* css support.
* modal windows and jscripts.

#Login procedure

* Session key is assigned for every sucessful login operation.
* Session key is carried in URL, for example: http://localhost/index.py?session\_key=f5e811cf01c58ad575526ad019a970c7bac6e9d9
* Session key has limited life time. 
* Session key life time extended with every new request.
* If session key has expired before the request, the user will be redirected to login.
* Session key is deleted during logout.

#The borsch.gateway interface
This interface allows to have dynamic web pages support. Currently it is only Python language, but the interface itself has no restrictions to use other popular laguages. It uses file extension to understand what kind of language is this.
How it works:

![picture](docs/img/virtual_terminal.png)

* When borsch.www receives request to process a python file with GET method it extracts all arguments from URL to python dictionary. If it is a POST request, it also add to this dictionary data from HTML forms.
* It encodes this dictionary to string which is valid python dictionary but in quotes.
* STDIN is used to carry full HTTP request from client to the script.
* Then borsch.www executes requested python script in a virtual terminal and pass this encoded dictionary as the first command line argument.
* When the script finished borsch.www reads stdout and stderr from this virtual terminal and sends back to the browser.
* STDOUT contains HTTP headers and body.
* STDERR contains errors.


For example in case of the following GET request:
```http
GET /topics.py?session_key=abc&theme_id=3 HTTP/1.1
```
arguments "session_key" and "theme_id" can be accessed from topics.py in the following way:
```python
from sys import argv
script, input_str = argv
input_data = eval(input_str)
request = sys.stdin.read()

print input_data['session_key'], input_data['theme_id']
```

##Limitations
 
* Scripts must not mannualy write anything to STDERR, it is reserved to Python exceptions.
* Borsch.talk search is case sensitive.

#Changelist

* 02/10/2016 borsch_0.1.1: inial release
* 16/10/2016 borsch_0.1.2: fixed issues
    * \#1: Background does not scale bottom.
    * \#3: Fill readme file.
    * \#5: Add more info in Topics view.
    * \#18: Fix underlines in index page.
    * \#21: Add panel "log in/sign in" to every page.
    * \#26: Move CSS to the separate file.
    * \#29: Max message length is a way too short.
    * \#30: Can you please adjust message text area.
* 14/11/2016 borsch_0.1.3: fixed issues
    * \#4: Sort Topics by date.
    * \#8: User avatar.
    * \#14: Search form.
    * \#16: Improve "Sign in" form.
    * \#17: Improve "Log in" form.
    * \#19: Change of datetime view.
    * \#20: View full path of current topic/theme.
    * \#23: Get rid off urlparse dependency.
    * \#34: Remove Pusheen_146.gif in new_topic.template.
    * \#36: None comment view.
    * \#37: Add "word-wrap" in messages page.
    * \#39: Fix POST download additional data.
    * \#40: Support POST multipart/form-data.
    * \#45: Improve view "Add Topic" page.
    * \#46: Change URL to index.py after "login" or "register".
* 24/01/2017 borsch_0.1.4: fixed issues:
    * \#10: In the URL change login by session key for more security.
    * \#11: Extend python HTTP headers.
    * \#25: Improve structure "server.talk".
    * \#28: Bug with login state.
    * \#35: Combine css' files.
    * \#48: Replace "print" by "sys.stdout.write".
    * \#51: HTML code instead HTML page.
    * \#52: Inprove add_topic.py.
    * \#53: In URL "add_topic.py" after adding new topic.
    * \#55: Error after re login after expired session.
* 21/07/2017 borsch_0.1.5: fixed issues:
    * \#7: Move to Ajax.
    * \#22: Profile page.
    * \#27: Content of topics and messages not centered.
    * \#42: Investigate browser behavior in case of '/' in the URL.
    * \#49: Fix for busy port which dont allow you to start server.py sometimes.
    * \#54: Change 'log in' and 'sign in' to modal windows.
    * \#56: Modal windows for "profile page" and "invalid data" (login).
    * \#58: Empty fields in "authlayer".
    * \#59: Status "File not Found".
    * \#62: First steps towards Ajax platform.
    * \#63: Second steps towards Ajax platform.
    * \#64: Profile.py: KeyError: 'name'.
    * \#65: Session_key for new register user.
    * \#66: sqlite3.IntegrityError: CHECK constraint failed: USERS.
    * \#67: Online/Offline icon.
    * \#68: Some testing Ajax/JS/Cookie.
    * \#69: 500 Internal Server Error.
    * \#70: Intro in Docker.
    * \#72: Tidy output.
    * \#73: Files cleanup.
    * \#74: Change labels buttons.
    * \#75: show_messages.py goes to infinite loop.
    * \#76: Search not working.
    * \#77: Add new topic doesn't work.
    * \#78: Profile doesn't work.
    * \#79: Encoding cyrillic symbols Ё & ё.
    * \#80: Borsch.talk dosn't work with expired key.
    * \#81: Update Readme.

