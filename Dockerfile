# Use an official Python runtime as a parent image
FROM python:2.7-slim

# Set the working directory to /borsch
WORKDIR /borsch/borsch.talk

# Copy the current directory contents into the container at /borsch
ADD . /borsch

# Install any needed packages specified in requirements.txt
# RUN pip install -r requirements.txt

# Make port 80 available to the world outside this container
EXPOSE 80

# Define environment variable
ENV PYTHONPATH /borsch

# Run server.py when the container launches
CMD ["python", "-u", "../borsch.www/server.py"]
