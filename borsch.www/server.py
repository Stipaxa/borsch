import os
import socket
import subprocess
from datetime import datetime
import myhttp
import utils
from borsch import http
import select


#---------------------------------------------------------------------------
# Create HTTP socket and listen one connection.
#---------------------------------------------------------------------------
def createHTTPSocket(host, port):
    addr = (host, port)
    socket_http = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket_http.bind(addr)
    socket_http.listen(5)
    return socket_http

#---------------------------------------------------------------------------
# Create connection
#---------------------------------------------------------------------------
def createConnection(socket):
    utils.log("Waiting for incoming connection...")
    connection, client_addr = socket.accept()
    utils.log("Connection established from {0}".format(client_addr))
    return connection, client_addr

def prepToProcess(request, connection):
    # Step 1: set default values for our variables
    data = ''
    ct = 'text/html; charset=UTF-8'
    
    # Step 2: Parse and form data
    parsed_line_req = myhttp.parseRequestLine(request)
    file_name = parsed_line_req[1][1:].split('?')[0]
    file_type = utils.getFileExt(file_name)
    headers = myhttp.basicResponseHeaders('HTTP/1.1', '200', 'OK')
    return file_name, file_type

#---------------------------------------------------------------------------
# Process GET request
#---------------------------------------------------------------------------
def processGet(request, connection):
    # Step 1: set default values for our variables
    data = ''
    ct = 'text/html; charset=UTF-8'
    
    # Step 2: Parse and form data
    headers = myhttp.basicResponseHeaders('HTTP/1.1', '200', 'OK')
    parsed_line_req = myhttp.parseRequestLine(request)
    file_name = parsed_line_req[1][1:].split('?')[0]
    if not file_name:
        file_name = 'redirect.py'

    file_type = utils.getFileExt(file_name)
    isfile = os.path.isfile(file_name)
    
    # Step 3: The main logic
    if isfile and file_type == 'py':
        url = request.split('\n')[0].split(' ')[1]
        try:
            url_data = url.split('?')[1]
        except IndexError:
            url_data = ''
        parsed_url = myhttp.parsePostUrlencoded(url_data)
        url_text = myhttp.convertDictToStr(parsed_url)
        data = utils.execFile(file_name, url_text, request)
        data_headers, data_body = http.splitHttp(data)
        data = data_body
        data_headers_dict = utils.getHeadersDict(data_headers)
        if 'Content-Type' in data_headers_dict:
            ct = data_headers_dict['Content-Type']
        headers.update(data_headers_dict)
    elif isfile:
        data = utils.readFile(file_name)
        ct = myhttp.contentTypeByFileExt(file_name)
    else: # Not found
        data = myhttp.page404()
        headers[':'] = 'HTTP/1.1 404 Not Found'

    # Step 4: Finalise and send data
    cl = len(data)
    headers_text = myhttp.makeTextHeaders(headers, ct, cl)
    utils.log(headers_text)
    connection.send(headers_text)
    connection.send(data)
    connection.close()

    # Step 5: Return HTTP code
    return headers[':'].split(' ')[1]

def processPost(request, connection):
    # How to use class
    # file_name = http_req.url.filename
    # if http_req.headers['Content-Type'] == 'www-form-urlencoded':
    #     dict_body = http_req.url.parseUrlEncoded(http_req.body)
    # else:
    #     dict_body = {}
    # dict_body.update(http_req.url.args)

    # http_req.addMoreData(data) self.data += data

    # saveToFile('aaa.png', http_req.body)
    

    file_name, file_type = prepToProcess(request, connection)
    headers = myhttp.basicResponseHeaders('HTTP/1.1', '200', 'OK')

    post_body = request.split('\n')[-1] #last line of request = users details
    dict_body = myhttp.parsePostUrlencoded(post_body)

    url = request.split('\n')[0].split(' ')[1]
    try:
        url_data = url.split('?')[1]
    except IndexError:
        url_data = ''
    dict_url = myhttp.parsePostUrlencoded(url_data)

    dict_body.update(dict_url) #combine 2 dicts dict_body += dict_url
    encoded_dict = myhttp.convertDictToStr(dict_body)
    data = utils.execFile(file_name, encoded_dict, request)
    ct = myhttp.contentTypeByFileExt(file_name)
    data_headers, data_body = http.splitHttp(data)
    data = data_body
    data_headers_dict = utils.getHeadersDict(data_headers)
    if 'Content-Type' in data_headers_dict:
        ct = data_headers_dict['Content-Type']
    headers.update(data_headers_dict)

    # Step 4: Finalise and send data
    cl = len(data)
    headers_text = myhttp.makeTextHeaders(headers, ct, cl)
    connection.send(headers_text)
    connection.send(data)
    connection.close()

    # Step 5: Return HTTP code
    return headers[':'].split(' ')[1]

def processPut(request, connection):
    file_name = 'push.jpg'
    file_type = utils.getFileExt(file_name)
    headers = myhttp.basicResponseHeaders('HTTP/1.1', '200', 'OK')
    data = utils.readFile(file_name)
    ct = myhttp.contentTypeByFileExt(file_name)
    cl = len(data)
    headers_text = myhttp.makeTextHeaders(headers, ct, cl)
    connection.send(headers_text)
    connection.send(data)
    connection.close()

    # Step 5: Return HTTP code
    return headers[':'].split(' ')[1]

def downloadData(connection, sz):
    #sz - size of data to download
    data = ''
    while sz > 0:
        new_data = connection.recv(sz)
        sz -= len(new_data)
        data += new_data
    return data

def strDecode(a_str):
    r = [c if (ord(c)<128 and ord(c)>=32) or ord(c)==10 or ord(c)==13 else "."  for c in a_str]
    return ''.join(r)

#---------------------------------------------------------------------------
# Tne entry point
#---------------------------------------------------------------------------
def Main():
    # server_host = 'localhost'
    server_host = '0.0.0.0'
    server_port = 80
    server_socket = createHTTPSocket(server_host,server_port)
    print "* Running on http://{0}:{1} (Press CTRL+C to quit)".format(server_host, server_port)

    while True:
        try:
            connection, client_addr = createConnection(server_socket)

            # request = connection.recv(1024*3)
            connection.setblocking(0)
            ready = select.select([connection], [], [], 3)
            if ready[0]:
                request = connection.recv(4096)
            else:
                connection.close()
                continue

            if len(request) == 0:
                utils.log("Invalid request, wait for the next request")
                connection.close()
                continue

            time = datetime.utcnow().strftime('%a, %d %b %Y %H:%M:%S GMT')
            method, url, version = myhttp.parseRequestLine(request)
            http_req_line = ' '.join([method, url, version.strip()])
            code = 'N/A' #Method not supported
            if method == 'POST':
                cl = 0
                req_lines = request.split('\n')
                for hdr in req_lines:
                    if hdr.startswith('Content-Length'):
                        cl = int(hdr.split(': ')[1])
                        break
                for i, hdr in enumerate(req_lines):
                    if hdr == '\r' or hdr == '':
                        body = '\n'.join(req_lines[(i+1):])
                        break

                #body = req_lines[-1]
                #length of data to download to complete POST request
                sz = cl - len(body)                
                if sz > 0:
                    data = downloadData(connection, sz)
                    request += data
                code = processPost(request, connection)
            elif method == 'GET':
                code = processGet(request, connection)
            elif method == 'PUT':
                code = processPut(request, connection)
            else:
                utils.log("Not supported {0} method".format(method))
                connection.close()
            print '{0} - - [{1}] "{2}" {3} - '.format(client_addr[0], time, http_req_line, code)
        except:
            connection.close()
            raise

if __name__ == '__main__':
    Main()
