import os
import subprocess
from datetime import datetime
import utils

#-----------------------------------------------------------------------------
# The function which returns file's content type in HTTP format.
#-----------------------------------------------------------------------------
def contentTypeByFileExt(file_name):
    #step 1: define supported content types
    content_type = {
        'py': 'text/html; charset=UTF-8',
        'ico': 'image/x-icon',
        'png': 'image/png',
        'gif': 'image/gif',
        'jpg': 'image/jpeg',
        'css': 'text/css',
        'html': 'text/html; charset=UTF-8'}
    #step 2: get extension from file
    ext = utils.getFileExt(file_name)
    #ext = os.path.splitext(file_name)[1][1:]
    #step 3: return content_type by the extension
    try:
        return content_type[ext]
    except KeyError:
        return 'application/octet-stream'

#-----------------------------------------------------------------------------
# The function which returns response's headers in dictionary format
#-----------------------------------------------------------------------------
def basicResponseHeaders(http_ver, code, status):
    headers = {
            ':': '{0} {1} {2}'.format(http_ver, code, status),
            'Server': 'Borsch',
            'Connection': 'close'}
    return headers

#-----------------------------------------------------------------------------
# The function which returns headers in HTML format
#-----------------------------------------------------------------------------
def makeTextHeaders(headers, content_type, content_length):
    time = datetime.utcnow().strftime('%a, %d %b %Y %H:%M:%S GMT')
    headers['Date'] = time
    result = headers[':'] + '\r\n'
    for k, v in headers.iteritems():
        if k == ':':
            continue
        if k == 'Content-Type' and content_type != None:
            continue
        if k == 'Content-Length' and content_length != None:
            continue
        result += k + ': ' + v + '\r\n'

    if content_type != None:
        result += 'Content-Type: ' + content_type + '\r\n'
        headers['Content-Type'] = content_type
    if content_length != None:
        result += 'Content-Length: ' + str(content_length) + '\r\n'
        headers['Content-Length'] = str(content_length)
    # result += 'Cookies: myName=mums_cookies;'
    # headers['Cookies'] = 'mums_cookies'
    result += '\r\n'
    return result


#-----------------------------------------------------------------------------
# The function which create/return HTML page when happen error "404 Not Found"
#-----------------------------------------------------------------------------
def page404():
    html = "<html><body>"
    html += "<font size='36' color='brown' face='Arial'>"
    html += "404 File Not Found</font><br>"
    html += "<IMG src='../images/lemur.jpg' alt='Picture' width='527' height='361' />"
    html += "</body></html>"
    return html

#-----------------------------------------------------------------------------
# The function which returns parsed 1st request line in list format
# (parsed by ' ')
#-----------------------------------------------------------------------------
def parseRequestLine(request):
    line_n = request.split('\n')
    line_sp = line_n[0].split(' ')
    return line_sp

#-----------------------------------------------------------------------------
# The function create HTML page and print url args and url values
#-----------------------------------------------------------------------------
def makeUrlAVText(url_dict):
    html = '<html><body>'
    for arg, value in url_dict.items():
        html += '{0} is {1}'.format(arg, value[0])
        html += '<br>'
    html += '</body></html>'
    return html

#-----------------------------------------------------------------------------
# The function which returns url args and values in string
# with dictionary format
#-----------------------------------------------------------------------------
def convertDictToStr(url_dict):
    str_tmp = []
    for arg, value in url_dict.iteritems():
        str_tmp.append('"{0}":"{1}"'.format(arg, value))
    url_str = '{' + ','.join(str_tmp) + '}'
    return url_str

def parsePostUrlencoded(url_data):
    if not url_data:
        return {}
    tmp = url_data.split('&')
    d = {}
    for i in tmp:
        i_tmp = i.split('=')
        k = i_tmp[0]
        v = i_tmp[1]
        d[k] = v
    return d

"""
class Url(object):
    def __init__(self, req_line, host):
        self.host = host
        self.filename = ''
        self.parse(req_line) # -> self.


    def parse(self, req_line):
        ...
        self.filename = ' . '
        self.text = ' . '

    @static
    def parseUrlEncoded(url_data):
        return {}


url = Url('GET /index.py?login=nnn HTTP/1.1', '192.168.0.27:8080')
print url.filename
print url.text -> '192.168.0.27:8080/index.py?login=nnn'
print url.args -> {login:"nnn"}

class HttpRequest(object):
    def __init__(self, bytes_from_socket):
        self.data = bytes_from_socket # data = req line + headers + \r\n + body
        first_line = split()
        self.headers = wunder_uafflya()
        self.url = Url(first_line, self.headers['Host'])

    def addMoreData(self, more_data):
        self.data += more_data 
"""


