# -*- coding: utf-8 -*-
import os
import subprocess

def log(message):
    # print message
    pass

#---------------------------------------------------------------------------
# Utility function which returns file's size.
#---------------------------------------------------------------------------
def fileSize(file_name):
    f_size = os.path.getsize(file_name)
    return f_size

#----------------------------------------------------------------------------
# Utility function which returns file content.
#----------------------------------------------------------------------------
def readFile(file_name):
    file_data = ""
    if os.path.isfile(file_name):
        f_size = fileSize(file_name)
        with open(file_name,'r') as f:
            file_data = f.read(f_size)
    return file_data

#-----------------------------------------------------------------------------
# Utility function which returns file extention.
# In case of no ext it returns ''.
#
# NOTE: All functions that start from _ are internal for this module only
# and not supposed to be called from another module.
#-----------------------------------------------------------------------------
def getFileExt(file_name):
    ext = ''
    parsed_name = file_name.split('.')
    if len(parsed_name) > 1:
        ext = parsed_name[-1]
    else:
        ext = ''
    return ext

def doServerResponse_500():
    rsp = 'HTTP/1.1 500 Internal Server Error\r\n'
    rsp += '\r\n'
    rsp += '******RUNTIMEERROR*******'
    return rsp

#-----------------------------------------------------------------------------
# The function which returns stdout running "python file_name args"
#-----------------------------------------------------------------------------
def execFile(file_name, args, request):
    if args == None:
        shell_command = "python " + file_name
    else:
        shell_command = "python " + file_name + " " + "'" + args + "'"

    v_term = subprocess.Popen([shell_command], 
                              shell=True, 
                              stdout=subprocess.PIPE, 
                              stderr=subprocess.PIPE,
                              stdin=subprocess.PIPE)
    v_term_stdout, v_term_stderr = v_term.communicate(input=request)
    log("STDERR="+v_term_stderr)
    if 'Traceback (most recent call last):' in v_term_stderr:
        return doServerResponse_500()
    return v_term_stdout

def getHeadersDict(req):
    hdr_dict = {}
    hdrs = req.split('\n')
    if ': ' not in hdrs[0]:
        if hdrs[0] != '':
            hdr_dict[':'] = hdrs[0]
        hdrs = hdrs[1:]

    for line in hdrs:
        if line == '\r' or line == '':
            break
        line_tmp = line.split(': ')
        k = line_tmp[0]
        v = line_tmp[1]
        hdr_dict[k] = v
    return hdr_dict 
